﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerService.Models
{
    public class ClientsDue
    {
        public string names { get; set; }
        public string accountnumber { get; set; }
        public decimal amounts { get; set; }
    }
    public class securitytotals
    {
        public string TotalRegistration { get; set; }
        public string totalfunds { get; set; }
        public string CDCHOLDINGS { get; set; }
        public string totalholdings { get; set; }
        public string Incomingsells { get; set; }
        public string Incomingbuys { get; set; }
        public string Pendingsettlement { get; set; }
        public string todaysettlement { get; set; }
    }
    public class Brokers
    {
        public string Name { get; set; }
        public string CDSNo { get; set; }
        public string Phone { get; set; }
        public string Broker { get; set; }
        public string IDNumber { get; set; }
        public string IDType { get; set; }
        public string AccountType { get; set; }
        public string TradingStatus { get; set; }
    }
    public class marketwatch
    {

        public string company { get; set; }


        public string Volume { get; set; }


        public string Bid { get; set; }


        public string VolumeSell { get; set; }


        public string Ask { get; set; }


        public string LastTradedPrice { get; set; }


        public string Lastmatched { get; set; }


        public string lastvolume { get; set; }


        public string TotalVolume { get; set; }


        public string Turnover { get; set; }


        public string Open { get; set; }


        public string High { get; set; }


        public string Low { get; set; }


        public string AveragePrice { get; set; }


        public string Change { get; set; }


        public string percchange { get; set; }


        public string url { get; set; }


        public string url2 { get; set; }

    }

    public class marketwatchzse
    {
        public string Ticker { get; set; }
        public string ISIN { get; set; }
        public string Best_Ask { get; set; }
        public string Best_bid { get; set; }
        public string Current_price { get; set; }
    }
    public class loadbuys
    {
        public string Company { get; set; }
        public string Quantity { get; set; }
        public string BasePrice { get; set; }
        public string OrderPref { get; set; }
        public string TIF { get; set; }
        public string Block { get; set; }
        public string mine { get; set; }
    }
    public class loadsells
    {
        public string Company { get; set; }
        public string Quantity { get; set; }
        public string BasePrice { get; set; }
        public string OrderPref { get; set; }
        public string TIF { get; set; }
        public string Block { get; set; }
        public string mine { get; set; }
    }
}