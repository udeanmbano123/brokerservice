    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
namespace BrokerService.Models
{

    public class tblMatchedOrders
    {
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [StringLength(50)]
        public string TimeStamp { get; set; }

        [Column(TypeName = "money")]
        public decimal? TradePrice { get; set; }

        public double? TradeQty { get; set; }

        [StringLength(50)]
        public string ReportID { get; set; }

        [StringLength(50)]
        public string Side1 { get; set; }

        [StringLength(50)]
        public string OrderID1 { get; set; }

        [StringLength(50)]
        public string PartyID1 { get; set; }

        [StringLength(50)]
        public string Source1 { get; set; }

        [StringLength(50)]
        public string Role1 { get; set; }

        [StringLength(50)]
        public string Account1 { get; set; }

        [StringLength(50)]
        public string Side2 { get; set; }

        [StringLength(50)]
        public string OrderID2 { get; set; }

        [StringLength(50)]
        public string PartyID2 { get; set; }

        [StringLength(50)]
        public string Source2 { get; set; }

        [StringLength(50)]
        public string Role2 { get; set; }

        [StringLength(50)]
        public string Account2 { get; set; }

        [StringLength(50)]
        public string BankSent { get; set; }

        [StringLength(50)]
        public string BankRef { get; set; }

        [StringLength(50)]
        public string AccRef1 { get; set; }

        [StringLength(50)]
        public string AccRef2 { get; set; }

        [StringLength(50)]
        public string Broker1 { get; set; }

        [StringLength(50)]
        public string Broker2 { get; set; }

        [StringLength(50)]
        public string Ack { get; set; }

        [StringLength(500)]
        public string Error_details { get; set; }

        [StringLength(50)]
        public string CommonRef { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_posted { get; set; }

        public int? SettlementCycle { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SettlementDate { get; set; }

        [StringLength(50)]
        public string Company { get; set; }

        [StringLength(50)]
        public string BSCREF { get; set; }

        [StringLength(50)]
        public string SSCREF { get; set; }

        [StringLength(50)]
        public string TRAREF { get; set; }

        public bool? AckSent { get; set; }
    }
}
