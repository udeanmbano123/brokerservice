﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerService.Models
{
    public class ClientsDue
    {
        public string names { get; set; }
        public string accountnumber { get; set; }
        public decimal amounts { get; set; }
    }
}