﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerService.Models
{
    public class CompanyEarners
    {
       public string companyname { get; set; }
      public string brokername { get; set; }
        public decimal consideration { get; set; }
        public decimal commission { get; set; }
        public string tradeType { get; set; }
    }
}