   using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
namespace BrokerService.Models
{
 

    public  class CompanyLivePrices
    {
        [StringLength(255)]
        public string COMPANY { get; set; }

        public double? BID { get; set; }

        public double? OFFER { get; set; }

        public double? CurrentPrice { get; set; }

        public double? ShareVOL { get; set; }

        public DateTime? LastUpdated { get; set; }
        [Key]
        public int id { get; set; }
    }
}
