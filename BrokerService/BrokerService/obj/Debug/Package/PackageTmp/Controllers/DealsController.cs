﻿using BrokerService.ATS;
using BrokerService.CDS;
using BrokerService.DAO;
using BrokerService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BrokerService.Controllers
{
    public class DealsController : ApiController
    {
        private GreenContext db = new GreenContext();
        private DSContext db2 = new DSContext();
        private CDS2Entities db3 = new CDS2Entities();
        private testCDSEntities db4 = new testCDSEntities();

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic Deals()
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals

                       let Deal = s.Deal
                       let BuyCompany = s.BuyCompany
                       let SellCompany = s.SellCompany
                       let Buyer = s.Buyercdsno
                       let Seller = s.Sellercdsno
                       let Quantity = s.Quantity
                       let Trade = s.Trade
                       let DealPrice = s.DealPrice
                       //where s.Buyercdsno==bss || s.Sellercdsno==bss
                       select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
            //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic DealsP()
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals
                       select s;           
               //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Matcheds")]
        [AllowAnonymous]
        public dynamic DealsPP()
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals
                       select s;
            return urls;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic ATSPrice(string date)
        {
            DateTime dt = Convert.ToDateTime(date);
            var urls4 = from s in db.CompanyLivePricess

                        let Company = s.COMPANY
                        let Bid = s.BID
                        let Offer = s.OFFER
                        let CurrentPrice = s.CurrentPrice
                        let ShareVolume = s.ShareVOL
                        let Date = s.LastUpdated
                        where DbFunctions.TruncateTime(s.LastUpdated) == dt
                        select new { Company, Bid, Offer, CurrentPrice, ShareVolume, Date };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic SettledDeals()
        {
            var urls2 = from s in db2.tblMatchedOrderss
                        let SID = s.ID
                        let TradePrice = s.TradePrice
                        let Quantity = s.TradeQty
                        let OrderNo = s.OrderID1
                        let Buyer = s.Account1
                        let OrderNo2 = s.OrderID2
                        let Seller = s.Account2
                        let Acknowledgement = s.Ack
                        let SettlementDate = s.SettlementDate


                        select new { SID, TradePrice, Quantity, OrderNo, Buyer, OrderNo2, Seller, Acknowledgement, SettlementDate };
            return urls2;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic SpinTrans(string p)
        {
            //int? my = Convert.ToInt32(p);
            var urls3 = from s in db2.TransactionCharges
                        let SID = s.Id
                        let Transcode = s.transactionCode
                        let ChargeCode = s.ChargeCode
                        let BuyCharges = s.BuyCharges
                        let SellCharges = s.SellCharges
                        let Date = s.Date
                        where s.transactionCode==p

                        select new { SID, Transcode, ChargeCode, BuyCharges, SellCharges, Date };
            return urls3;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<CompanyEarners> GetEarners(string date)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select  c.Company_name As 'Brokername',Broker1,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(BuyCharges),0) As 'Commission','Buy' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker1 where t.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and t.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt+"') + 1, 0)) group by Broker1,c.Company_name union all"
                + " (select  c.Company_name As 'Brokername',Broker2,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(SellCharges),0) As 'Commission','Sell' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker2 where t.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and t.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt + "') + 1, 0)) group by Broker2,c.Company_name)";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CompanyEarners>();
            while (reader.Read())
            {
                var accountDetails = new CompanyEarners
                {

                   companyname = reader.GetValue(0).ToString(),
                   brokername = reader.GetValue(1).ToString(),
                   consideration=Convert.ToDecimal(reader.GetValue(2).ToString()),
                   commission= Convert.ToDecimal(reader.GetValue(3).ToString()),
                   tradeType= reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<CompanyEarners> GetEarnersRange(string date1, string date2)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date1);
            DateTime dt2 = Convert.ToDateTime(date2);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select  c.Company_name As 'Brokername',Broker1,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(BuyCharges),0) As 'Commission','Buy' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker1 where t.[Date]>='" + dt + "'  and t.[Date]<='" + dt2 + "' group by Broker1,c.Company_name union all"
                + " (select  c.Company_name As 'Brokername',Broker2,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(SellCharges),0) As 'Commission','Sell' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker2 where t.[Date]>='" + dt + "'  and t.[Date]<= '" + dt2 + "' group by Broker2,c.Company_name)";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CompanyEarners>();
            while (reader.Read())
            {
                var accountDetails = new CompanyEarners
                {

                    companyname = reader.GetValue(0).ToString(),
                    brokername = reader.GetValue(1).ToString(),
                    consideration = Convert.ToDecimal(reader.GetValue(2).ToString()),
                    commission = Convert.ToDecimal(reader.GetValue(3).ToString()),
                    tradeType = reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<ClientsDue> AmountsDue(string date)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "  select  Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames As Names ,Account2,IsNull(sum(SellCharges),0) As Amounts from tblMatchedOrders inner join TransactionCharges on CAST(tblMatchedOrders.ID as nvarchar)=TransactionCharges.transactionCode inner join Accounts_Clients on Accounts_Clients.CDS_Number=tblMatchedOrders.Account2 where TransactionCharges.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and TransactionCharges.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt + "') + 1, 0)) group by Account2,Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<ClientsDue>();
            while (reader.Read())
            {
                var accountDetails = new ClientsDue
                {

                    names = reader.GetValue(0).ToString(),
                    accountnumber = reader.GetValue(1).ToString(),
                    amounts = Convert.ToDecimal(reader.GetValue(2).ToString())
                   
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<ClientsDue> AmountsDueRange(string date1, string date2)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date1);
            DateTime dt2 = Convert.ToDateTime(date2);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "  select  Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames As Names ,Account2,IsNull(sum(SellCharges),0) As Amounts from tblMatchedOrders inner join TransactionCharges on CAST(tblMatchedOrders.ID as nvarchar)=TransactionCharges.transactionCode inner join Accounts_Clients on Accounts_Clients.CDS_Number=tblMatchedOrders.Account2 where TransactionCharges.[Date]>='" + dt + "' and TransactionCharges.[Date]<= '" + dt2 + "' group by Account2,Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<ClientsDue>();
            while (reader.Read())
            {
                var accountDetails = new ClientsDue
                {

                    names = reader.GetValue(0).ToString(),
                    accountnumber = reader.GetValue(1).ToString(),
                    amounts = Convert.ToDecimal(reader.GetValue(2).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BrokersList")]
        [AllowAnonymous]
        public dynamic Company()
        {

            var urls4 = from s in db3.Client_Companies
                         select new { s.Company_name,s.Company_Code };

            return urls4;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Broker/{v}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public dynamic CompanyCC(string v)
        {

            var urls4 = from s in db3.Client_Companies
                        where s.Company_name==v
                        select new { s.Company_name,s.Company_Code };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsList/{s}")]
        [AllowAnonymous]
        public dynamic Accounts(string s)
        {

            var urls4 = db3.Accounts_Audit.ToList().Where(a=>a.BrokerCode==s);

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsListUpdate/{s}/{p}")]
        [AllowAnonymous]
        public dynamic UploadAccounts(string s,string p)
        {
            int my = Convert.ToInt32(p);
            var urls4 = db3.Accounts_Audit.ToList().Where(a => a.BrokerCode == s && a.ID==my);

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("OrderBooks/{s}")]
        [AllowAnonymous]
        public dynamic OrderBook(string s)
        {
            var urls4 = db4.Tbl_MatchedDeals.ToList().Where(a => a.buybroker == s || a.sellbroker == s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Executed/{s}")]
        [AllowAnonymous]
        public dynamic Executed(string s)
        {
            var urls4 = db4.Tbl_MatchedDeals.ToList().Where(a => a.BuyCompany == s || a.SellCompany == s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Outstanding/{s}")]
        [AllowAnonymous]
        public dynamic Outs(string s)
        {
            var urls4 = db4.Order_Live.ToList().Where(a => a.Broker_Code==s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("denied/{s}")]
        [AllowAnonymous]
        public dynamic getA(string s)
        {
            var urls4 = db3.Accounts_Audit.ToList().Where(a => a.BrokerCode == s && a.AuthorizationState=="C");

            return urls4;
        }

   

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Order/{ordertype}/{CDS_Number}/{NoofNotes}/{TelephoneSelorder}/{Company}/{UserName}/{PassWord}/{MNO_}/{BasePrice}/{BeginDate}/{ExpiryDate}/{BrokerCode}/{MaxPrice}/{MiniPrice}/{shareholder}/{client}/{curr}/{tax}")]
        [AllowAnonymous]
        public string PostOrder(string ordertype,string CDS_Number, string NoofNotes, string TelephoneSelorder, string Company, string UserName, string PassWord, string MNO_, string BasePrice,string BeginDate ,string ExpiryDate,String BrokerCode,string MaxPrice,string MiniPrice,string shareholder,string client,string curr,string tax)
        {
            string response = "";
            string responseid = "";
            long max = 0;

            DateTime? my11 = Convert.ToDateTime(BeginDate);
            DateTime? my2 = Convert.ToDateTime(ExpiryDate);
            try
            {
                max = db4.Pre_Order_Live.ToList().Max(a => a.OrderNo);

            }
            catch (Exception)
            {

                max = 0;
            }
            max += 1;

            try
            {
                Pre_Order_Live my = new Pre_Order_Live();
                my.OrderType = ordertype.ToUpper();
                my.Company = Company;
                my.SecurityType = "EQUITY";
                my.CDS_AC_No = CDS_Number;
                my.Broker_Code = BrokerCode;
                my.Client_Type = "";
                my.Tax = 0;
                my.Shareholder = shareholder;
                my.ClientName = client;
                my.OrderStatus = "OPEN";
                my.Create_date = DateTime.Now;
                my.Deal_Begin_Date = my11;
                my.Expiry_Date = my2;
                my.Quantity = Convert.ToInt32(NoofNotes);
                my.BrokerRef = my.Broker_Code + "Ord"+(max - 1).ToString();
                my.OrderNumber = max.ToString();
                my.BasePrice = Convert.ToDecimal(BasePrice);
                my.AvailableShares = 0;
                my.OrderPref = "L";
                my.OrderAttribute = "";
                my.Marketboard = "Normal Board";
                my.TimeInForce = "Day Order (DO)";
                my.OrderQualifier = "None";
                my.ContraBrokerId = "None";
                my.MaxPrice = 0;
                my.MiniPrice =0;
                my.Flag_oldorder = false;
                my.TotalShareHolding = 0;
                my.Currency = "USD";
                my.Affirmation = true;
                my.FOK = false;
                db4.Pre_Order_Live.AddOrUpdate(my);
                db4.SaveChanges();
                var d = db4.Order_Live_Responses.ToList().Where(a=>a.OrderNo==max);
                foreach(var p in d)
                {
                    responseid = p.OrderNo.ToString();
                    response = p.Failed_Reason;
                }
                if (responseid =="")
                {
                    responseid = max.ToString();
                    response = "Order number "+ max.ToString() +" has been processed sucessfully";
                }
                return responseid+","+response;
            }
            catch (Exception)
            {

                return "0";
            }
           

        }
        [System.Web.Http.AcceptVerbs("PUT")]
        [System.Web.Http.HttpPut]
        [Route("OrderUpdate/{ordertype}/{CDS_Number}/{NoofNotes}/{TelephoneSelorder}/{Company}/{UserName}/{PassWord}/{MNO_}/{BasePrice}/{BeginDate}/{ExpiryDate}/{BrokerCode}/{MaxPrice}/{MiniPrice}/{shareholder}/{client}/{orderNo}/{curr}/{tax}")]
        [AllowAnonymous]
        public string PostOrderUpdate(string ordertype, string CDS_Number, string NoofNotes, string TelephoneSelorder, string Company, string UserName, string PassWord, string MNO_, string BasePrice, string BeginDate, string ExpiryDate, String BrokerCode, string MaxPrice, string MiniPrice, string shareholder, string client,string orderNo,string curr,string tax)
        {
            string response = "";
            long cs = Convert.ToInt32(orderNo);
            long max = 0;
            try
            {
                max = db4.Order_Live.ToList().Where(a => a.OrderNo == cs).Count();

            }
            catch (Exception)
            {

                max = 0;
            }
            DateTime? my11 = Convert.ToDateTime(BeginDate);
            DateTime? my2 = Convert.ToDateTime(ExpiryDate);
            string base1 = BasePrice.ToString().Replace("T", ".");
            string max2 = MaxPrice.ToString().Replace("T", ".");
            string mini2 = MiniPrice.ToString().Replace("T", ".");
            string tax2 = tax.ToString().Replace("T", ".");
            if (max > 0)
            {
                
                    Order_Live my = db4.Order_Live.Find(cs);
                my.OrderType = ordertype.ToUpper();
                my.Company = Company;
                    my.SecurityType = "EQUITY";
                    my.CDS_AC_No = CDS_Number;
                    my.Broker_Code = BrokerCode;
                    my.Client_Type = "N/A";
                    my.Tax = 0;
                    my.Shareholder = shareholder;
                    my.ClientName = client;
                    my.OrderStatus = "OPEN";
                    my.Deal_Begin_Date = my11;
                    my.Expiry_Date = my2;
                    my.Quantity = Convert.ToInt32(NoofNotes);
                    my.BrokerRef = my.BrokerRef;
                    my.OrderNumber = orderNo;
                    my.BasePrice = Convert.ToDecimal(base1);
                    my.AvailableShares = 0;
                    my.OrderPref = my.OrderPref;
                    my.OrderAttribute =my.OrderAttribute;
                    my.Marketboard = "Normal Board";
                    my.TimeInForce = "Day Order (DO)";
                    my.OrderQualifier = "None";
                    my.ContraBrokerId = "None";
                my.MaxPrice = 0;
                my.MiniPrice = 0;
                my.Flag_oldorder = false;
                my.TotalShareHolding = 0;
                my.Currency = "USD";
                my.Affirmation = true;
                my.FOK = false;
                db4.Order_Live.AddOrUpdate(my);
                    db4.SaveChanges();
                    return orderNo;
              
            }
            else
            {
                return "Matched";
            }
        }


        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Test/{s}")]
        [AllowAnonymous]

        public dynamic good(string s)
        {
            string my = "sex";
            var url = "it works" + s;
            return url;
        }

        [System.Web.Http.AcceptVerbs("PUT")]
        [System.Web.Http.HttpPut]
        [Route("place/{s}")]
        [AllowAnonymous]
        public dynamic good2(string s)
        {
            DateTime? my = Convert.ToDateTime(s);
            var url = "it works" + s;
            return url;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("CheckOrder/{s}")]
        [AllowAnonymous]
        public string checkorders(string s)
        {

            int mys = Convert.ToInt32(s);
            int d = 0;
            try
            {
                d = db4.Order_Live.ToList().Where(a => a.OrderNo == mys).Count();
            }
            catch (Exception)
            {
                d = 0;
            }
           
            return d.ToString();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Orders/{s}")]
        [AllowAnonymous]
        public string orders(string s)
        {

            int mys = Convert.ToInt32(s);
            int d = 0;
            try
            {
                d = db4.Order_Live.ToList().Where(a => a.OrderNo == mys).Count();
            }
            catch (Exception)
            {
                d = 0;
            }

            return d.ToString();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Billing")]
        [AllowAnonymous]
        public dynamic updates()
        {

            var c = db3.para_Billing.ToList();

            return c;
        }

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Joint/{Surname}/{Forenames}/{IDNo}/{IDType}/{Nationality}/{DateOfBirth}/{Gender}/{CDSNo}/{email}")]
        [AllowAnonymous]
        public string AccountJoint(string Surname, string Forenames, string IDNo, string IDType, string Nationality, string DateOfBirth, string Gender, string CDSNo, string email)
        {
            string message = "Fail";
            int d = 0;

            try
            {
                d = db3.Accounts_Joint.ToList().Where(a => a.Forenames == Forenames && a.Surname == Surname && a.IDNo == IDNo && a.CDSNo == CDSNo).Count();

            }
            catch (Exception)
            {

                d = 0;
            }
            if (d == 0)
            {
                Accounts_Joint p = new Accounts_Joint();
                p.Forenames = Forenames;
                p.Surname = Surname;
                p.DateOfBirth = Convert.ToDateTime(DateOfBirth);
                p.Gender = Gender;
                p.IDNo = IDNo;
                p.IDType = IDType;
                p.email = email;
                p.Nationality = Nationality;
                p.CDSNo = CDSNo;
                db3.Accounts_Joint.AddOrUpdate(p);
                db3.SaveChanges();
                message = "Successfully";
            }
            return message;
        }
    }
}
