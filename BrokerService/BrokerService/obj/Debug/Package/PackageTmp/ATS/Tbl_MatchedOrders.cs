//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_MatchedOrders
    {
        public long ID { get; set; }
        public Nullable<long> Deal { get; set; }
        public string BuyCompany { get; set; }
        public string SellCompany { get; set; }
        public string Buyercdsno { get; set; }
        public string Sellercdsno { get; set; }
        public Nullable<decimal> Quantity { get; set; }
        public Nullable<System.DateTime> Trade { get; set; }
        public Nullable<decimal> DealPrice { get; set; }
        public string DealFlag { get; set; }
        public string instrument { get; set; }
    }
}
