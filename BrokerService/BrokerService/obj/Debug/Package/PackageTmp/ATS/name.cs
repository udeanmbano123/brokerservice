//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class name
    {
        public decimal Shareholder { get; set; }
        public string Broker_Code { get; set; }
        public string CDS_Number { get; set; }
        public string Surname { get; set; }
        public string Forenames { get; set; }
        public string Idpp { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string nominee_code { get; set; }
        public string Title { get; set; }
        public string Add_1 { get; set; }
        public string Add_2 { get; set; }
        public string Add_3 { get; set; }
        public string Add_4 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Cellphone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Bank_Name { get; set; }
        public string Bank_Code { get; set; }
        public string Branch_Name { get; set; }
        public string Branch_Code { get; set; }
        public string Account_Type { get; set; }
        public string Account { get; set; }
        public Nullable<decimal> Tax { get; set; }
        public Nullable<bool> Mandate { get; set; }
        public Nullable<bool> hfc { get; set; }
        public Nullable<bool> Locked { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public string Industry { get; set; }
        public Nullable<bool> Approved { get; set; }
        public string Approved_by { get; set; }
        public Nullable<System.DateTime> Approved_on { get; set; }
        public string Holder_type { get; set; }
        public string ImageID { get; set; }
    }
}
