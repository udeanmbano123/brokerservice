//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class ActivityLog
    {
        public int ActivityLogID { get; set; }
        public int Uid { get; set; }
        public string Activity { get; set; }
        public string PageUrl { get; set; }
        public string IpAddress { get; set; }
        public Nullable<System.DateTime> ActivityDate { get; set; }
    }
}
