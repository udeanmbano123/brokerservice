﻿using BrokerService.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;




namespace BrokerService.DAO
{
    public class GreenContext : DbContext
    {
        public GreenContext() : base("GreenContext")
        {
        }


        //public virtual DbSet<Tbl_MatchedDeals> Tbl_MatchedDeals { get; set; }
        public  DbSet<CompanyLivePrices> CompanyLivePricess { get; set; }
     
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //one-to-many relationships

        
      
            base.OnModelCreating(modelBuilder);
        }



     

        //  public System.Data.Entity.DbSet<investorpage.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}
