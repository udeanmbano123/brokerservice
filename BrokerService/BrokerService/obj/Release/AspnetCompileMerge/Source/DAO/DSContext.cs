﻿using BrokerService.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace BrokerService.DAO
{
    public class DSContext : DbContext
    {
        public DSContext() : base("DSContext")
        {
        }


        public virtual DbSet<tblMatchedOrders> tblMatchedOrderss { get; set; }
        public virtual DbSet<TransactionCharges> TransactionCharges { get; set;}
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //one-to-many relationships



            base.OnModelCreating(modelBuilder);
        }




}
}