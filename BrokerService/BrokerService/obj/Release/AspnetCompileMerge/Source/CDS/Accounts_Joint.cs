//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.CDS
{
    using System;
    using System.Collections.Generic;
    
    public partial class Accounts_Joint
    {
        public int id { get; set; }
        public string Surname { get; set; }
        public string Forenames { get; set; }
        public string IDNo { get; set; }
        public string IDType { get; set; }
        public string Nationality { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string CDSNo { get; set; }
        public string email { get; set; }
    }
}
