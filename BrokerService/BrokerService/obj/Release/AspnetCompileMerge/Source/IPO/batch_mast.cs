//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.IPO
{
    using System;
    using System.Collections.Generic;
    
    public partial class batch_mast
    {
        public string batch_no { get; set; }
        public string batch_type { get; set; }
        public Nullable<decimal> app_shares { get; set; }
        public Nullable<decimal> app_cash { get; set; }
        public string status { get; set; }
        public string Company { get; set; }
        public string Agent { get; set; }
        public string BrokerBatchRef { get; set; }
        public Nullable<bool> Verify { get; set; }
        public string CreatedBy { get; set; }
        public string apps { get; set; }
        public string branch { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string PayMode { get; set; }
        public decimal ID { get; set; }
        public Nullable<decimal> BoxNo { get; set; }
        public string OldBatchRef { get; set; }
        public string Source { get; set; }
    }
}
