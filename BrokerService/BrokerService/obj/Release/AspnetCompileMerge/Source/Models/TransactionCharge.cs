using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace BrokerService.Models
{


    public class TransactionCharges
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string transactionCode { get; set; }

        [StringLength(50)]
        public string ChargeCode { get; set; }

        [Column(TypeName = "money")]
        public decimal? BuyCharges { get; set; }

        [Column(TypeName = "money")]
        public decimal? SellCharges { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date { get; set; }
    }
    public class Sealer
    {
        public string Security { get; set; }
        public string Deal { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string Account1 { get; set; }
        public string Account2 { get; set; }
        public string DatePosted { get; set; }
        public string SettlementDate { get; set; }
        public string Ack { get; set; }
        public string AccountName1 { get; set; }
        public string AccountName2 { get; set; }
        public string SecurityName { get; set; }
    }
}
