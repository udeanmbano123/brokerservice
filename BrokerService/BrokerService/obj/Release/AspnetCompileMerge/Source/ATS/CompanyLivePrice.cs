//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class CompanyLivePrice
    {
        public string COMPANY { get; set; }
        public Nullable<double> BID { get; set; }
        public Nullable<double> OFFER { get; set; }
        public Nullable<double> CurrentPrice { get; set; }
        public Nullable<double> ShareVOL { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public int id { get; set; }
    }
}
