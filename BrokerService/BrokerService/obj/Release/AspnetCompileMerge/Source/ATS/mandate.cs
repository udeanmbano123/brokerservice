//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class mandate
    {
        public decimal id { get; set; }
        public decimal Shareholder { get; set; }
        public string Man_LName { get; set; }
        public string Man_Forenames { get; set; }
        public string Man_ID { get; set; }
        public string Man_add1 { get; set; }
        public string Man_add2 { get; set; }
        public string Man_add3 { get; set; }
        public string Man_add4 { get; set; }
        public string Man_city { get; set; }
        public string Man_country { get; set; }
        public string man_tel { get; set; }
        public string man_cel { get; set; }
        public string man_fax { get; set; }
        public string man_email { get; set; }
        public string Man_bank { get; set; }
        public string Man_branch { get; set; }
        public string Man_account { get; set; }
        public string Man_AccType { get; set; }
    }
}
