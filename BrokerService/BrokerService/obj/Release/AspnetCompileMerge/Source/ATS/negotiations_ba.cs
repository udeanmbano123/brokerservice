//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class negotiations_ba
    {
        public int id { get; set; }
        public string Purchase_broker { get; set; }
        public string Sell_broker { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<System.DateTime> Datetime { get; set; }
        public Nullable<bool> Status { get; set; }
        public string buy_shareholder { get; set; }
        public string sell_shareholder { get; set; }
        public string ba_ref { get; set; }
        public Nullable<decimal> ba_price { get; set; }
    }
}
