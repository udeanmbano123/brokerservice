//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BrokerService.ATS
{
    using System;
    using System.Collections.Generic;
    
    public partial class ATS_Orders_Imports
    {
        public long TransID { get; set; }
        public long InitDealNo { get; set; }
        public Nullable<int> InitSuffixNo { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> DealPrice { get; set; }
        public Nullable<System.DateTime> DealDateTime { get; set; }
        public string Broker_Init { get; set; }
        public string Broker_Target { get; set; }
        public string Company { get; set; }
        public string InitDealType { get; set; }
        public string TargetDealType { get; set; }
        public Nullable<long> TargetDealNo { get; set; }
        public Nullable<int> TargetSuffixNo { get; set; }
    }
}
