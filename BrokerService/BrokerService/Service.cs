﻿using BrokerService.CDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrokerService
{
    public class Service
    {
        private CDS2Entities db3 = new CDS2Entities();
        public string Name(string s)
        {
            string uv = "";
            var pp = db3.Accounts_Audit.ToList().Where(a => a.CDS_Number == s);

            foreach (var k in pp)
            {
                uv = k.Surname + " " + k.Forenames;
            }
            if (uv == "")
            {
                uv = "No Name";
            }
            return uv;
        }
        public string NameC(string s)
        {
            string uv = "";
            var pp = db3.para_company.ToList().Where(a => a.Company == s);

            foreach (var k in pp)
            {
                uv = k.Fnam;
            }
            if (uv == "")
            {
                uv = "No Name";
            }
            return uv;
        }

    }
}