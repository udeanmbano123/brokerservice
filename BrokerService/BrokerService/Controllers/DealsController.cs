﻿using BrokerService.ATS;
using BrokerService.ATTS;
using BrokerService.CDS;
using BrokerService.DAO;
using BrokerService.IPO;
using BrokerService.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BrokerService.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")] // tune to your needs
    public class DealsController : ApiController
    {
        private GreenContext db = new GreenContext();
        private DSContext db2 = new DSContext();
        private CDS2Entities db3 = new CDS2Entities();
        private testCDSEntities db4 = new testCDSEntities();
        private floatEntities db5 = new floatEntities();
        private testcdsEntities2 db6 = new testcdsEntities2();
        public SqlDataAdapter adp;
        public SqlCommand cmd;
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic Deals()
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals

                       let Deal = s.Deal
                       let BuyCompany = s.BuyCompany
                       let SellCompany = s.SellCompany
                       let Buyer = s.Buyercdsno
                       let Seller = s.Sellercdsno
                       let Quantity = s.Quantity
                       let Trade = s.Trade
                       let DealPrice = s.DealPrice
                       //where s.Buyercdsno==bss || s.Sellercdsno==bss
                       select new { Deal, BuyCompany, SellCompany, Buyer, Seller, Quantity, Trade, DealPrice };
            //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic OrderUpdates()
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals
                       select s;
            //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("OrderUpdates/{f}")]
        [AllowAnonymous]
        public dynamic Pre(string f)
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Pre_Order_Live
                       where s.Broker_Code==f
                       select new{s.OrderNo,s.OrderStatus,s.trading_platform };           
               //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BuySell/{a}/{b}/{c}/{d}")]
        [AllowAnonymous]
        public dynamic Pren(string a,string b,string c,string d)
        {
            try
            {
                Para_Buy_Sell_Limits nn = new Para_Buy_Sell_Limits();
                nn.buy_limit = Convert.ToDecimal(a);
                nn.sell_limit = Convert.ToDecimal(b);
                nn.Reaction = c.Replace("-", "");
                nn.Custodian = d;
                db3.Para_Buy_Sell_Limits.Add(nn);
                db3.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {

                return "Failed";
            }

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BuySellU/{a}/{b}/{c}/{d}")]
        [AllowAnonymous]
        public dynamic Prenn(string a, string b, string c, string d)
        {
            try
            {
                var tru = 0;
                var fc = db3.Para_Buy_Sell_Limits.ToList().Where(y=>y.Custodian==d);
                foreach (var p in fc)
                {
                    tru = p.id;
                }
                Para_Buy_Sell_Limits nn = db3.Para_Buy_Sell_Limits.Find(tru);
                nn.buy_limit = Convert.ToDecimal(a);
                nn.sell_limit = Convert.ToDecimal(b);
                nn.Reaction = c.Replace("-", "");
                nn.Custodian = d;
                db3.Para_Buy_Sell_Limits.AddOrUpdate(nn);
                db3.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {

                return "Failed";
            }

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BuySellB/{a}/{b}/{c}/{d}")]
        [AllowAnonymous]
        public dynamic PrenB(string a, string b, string c, string d)
        {
            try
            {
                Para_Minimum_Limits nn = new Para_Minimum_Limits();
                nn.buy_limit = Convert.ToDecimal(a);
                nn.sell_limit = Convert.ToDecimal(b);
                nn.Reaction = c.Replace("-", "");
                nn.Custodian = d;
                nn.Date_set = DateTime.Now;
                db3.Para_Minimum_Limits.Add(nn);
                db3.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {

                return "Failed";
            }

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BuySellUB/{a}/{b}/{c}/{d}")]
        [AllowAnonymous]
        public dynamic PrennB(string a, string b, string c, string d)
        {
            try
            {
                var tru = 0;
                var fc = db3.Para_Minimum_Limits.ToList().Where(y => y.Custodian == d);
                foreach (var p in fc)
                {
                    tru = p.id;
                }
                Para_Minimum_Limits nn = db3.Para_Minimum_Limits.Find(tru);
                nn.buy_limit = Convert.ToDecimal(a);
                nn.sell_limit = Convert.ToDecimal(b);
                nn.Reaction = c.Replace("-", "");
                nn.Custodian = d;
                db3.Para_Minimum_Limits.AddOrUpdate(nn);
                db3.SaveChanges();
                return "Success";
            }
            catch (Exception)
            {

                return "Failed";
            }

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("External/{f}")]
        [AllowAnonymous]
        public dynamic Pref(string f)
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Pre_Order_Live
                       where s.Broker_Code == f
                       select s;
            //var urls = db.Tbl_MatchedDeals.Select(i =>
            //   new { i.Deal, i.BuyCompany, i.SellCompany, i.Buyercdsno, i.Sellercdsno,i.Quantity,i.Trade,i.DealPrice });
            return urls;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Matcheds")]
        [AllowAnonymous]
        public dynamic DealsPP(string zee)
        {

            //var returnRes = db.Tbl_MatchedDeals;

            var urls = from s in db4.Tbl_MatchedDeals
                       select s;
            return urls;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic ATSPrice(string date)
        {
            DateTime dt = Convert.ToDateTime(date);
            var urls4 = from s in db.CompanyLivePricess

                        let Company = s.COMPANY
                        let Bid = s.BID
                        let Offer = s.OFFER
                        let CurrentPrice = s.CurrentPrice
                        let ShareVolume = s.ShareVOL
                        let Date = s.LastUpdated
                        where DbFunctions.TruncateTime(s.LastUpdated) == dt
                        select new { Company, Bid, Offer, CurrentPrice, ShareVolume, Date };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic SettledDeals()
        {
            var urls2 = from s in db2.tblMatchedOrderss
                        let Security=s.Company
                        let Deal=s.ID
                        let Quantity=s.TradeQty
                        let Price=s.TradePrice
                        let Account1=s.Account1
                        let Account2=s.Account2
                        let DatePosted=s.Date_posted
                        let SettlementDate=s.SettlementDate
                        let Ack=s.Ack
                       select new { Security, Deal, Quantity, Price, Account1, Account2,DatePosted,SettlementDate,Ack };
            return urls2;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("settled/{id}")]
        [AllowAnonymous]
        public dynamic SettledDealsP(string id)
        {
            int my = Convert.ToInt32(id);
            var urls2 = from s in db2.tblMatchedOrderss
                        let Security = s.Company
                        let Deal = s.ID
                        let Quantity = s.TradeQty
                        let Price = s.TradePrice
                        let Account1 = s.Account1
                        let Account2 = s.Account2
                        let DatePosted = s.Date_posted
                        let SettlementDate = s.SettlementDate
                        let Ack = s.Ack
                        where s.ID==my
                        select new { Security, Deal, Quantity, Price, Account1, Account2, DatePosted, SettlementDate,Ack };
            return urls2;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("settleds")]
        [AllowAnonymous]
        public dynamic SettledDealsPP()
        {

            var urls2 = GetEarners();
            return urls2;
        }

        public static List<Sealer> GetEarners()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "  Select Company As 'Security',ID as 'Deal',TradeQty as 'Quantity',TradePrice As 'Price',Account1,Account2,Date_posted As 'DatePosted',SettlementDate,IsNull(Ack,'None') As Ack,(Select top 1 Surname + ' '+ Forenames from Accounts_Audit where CDS_Number=Account1) As 'AccountName1',(Select top 1 Surname + ' '+ Forenames from Accounts_Audit where CDS_Number=Account2) As 'AccountName2',(Select top 1 fnam from para_company where Company=tblMatchedOrders.Company) As 'SecurityName' from [dbo].[tblMatchedOrders]";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Sealer>();
            while (reader.Read())
            {
                var accountDetails = new Sealer
                {

        
                 Security= reader.GetValue(0).ToString(),
                   Deal = reader.GetValue(1).ToString(),
         Quantity = reader.GetValue(2).ToString(),
                  Price=reader.GetValue(3).ToString(),
          Account1 =reader.GetValue(4).ToString(),
          Account2 =reader.GetValue(5).ToString(),
         DatePosted =reader.GetValue(6).ToString(),
          SettlementDate =reader.GetValue(7).ToString(),
          Ack =reader.GetValue(8).ToString(),
          AccountName1=reader.GetValue(9).ToString(),
          AccountName2 =reader.GetValue(10).ToString(),
          SecurityName=reader.GetValue(11).ToString(),
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("GetName/{s}")]
        [AllowAnonymous]
        public string Name(string s)
        {
            string uv = "";
            var pp = db3.Accounts_Audit.ToList().Where(a => a.CDS_Number == s);

            foreach (var k in pp)
            {
                uv = k.Surname + " " + k.Forenames;
            }
            if (uv == "")
            {
                uv = "No Name";
            }
            return uv;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("PreReg/{s}")]
        [AllowAnonymous]
        public dynamic PreRegg(string s)
        {
            var c = db4.Pre_Order_Live_Rej.ToList().Where(a=>a.Broker_Code==s && a.OrderStatus=="OPEN" && a.Action_== "Authorize Transaction");

            return c;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("PreRegU/{s}")]
        [AllowAnonymous]
        public dynamic PreReggU(string s)
        {
            try
            {
                int my = Convert.ToInt32(s);
                var c = db4.Pre_Order_Live_Rej.Find(my);
                c.OrderStatus = "Authorized";
                db4.Pre_Order_Live_Rej.AddOrUpdate(c);
                db4.SaveChanges();
                return "Order Number "+ s + " has been successfully authorized";
            }
            catch (Exception f)
            {

                return f.Message;
            }
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Custodian/{s}")]
        [AllowAnonymous]
        public string Names(string s)
        { 
            string uv = "";
            var pp = from v in db3.Accounts_Clients
                     join ss in db3.Client_Companies on v.Custodian equals ss.Company_Code
                     let name = ss.Company_name + "," + ss.contact_email
                     select new {name };



            foreach (var k in pp)
            {
                uv = k.name;
            }
            if (uv == "")
            {
                uv = "None,None";
            }
            return uv;
        }
        public string NameC(string s)
        {
            string uv = "";
            var pp = db3.para_company.ToList().Where(a => a.Company == s);

            foreach (var k in pp)
            {
                uv = k.Fnam;
            }
            if (uv == "")
            {
                uv = "No Name";
            }
            return uv;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public dynamic SpinTrans(string p)
        {
            //int? my = Convert.ToInt32(p);
            var urls3 = from s in db2.TransactionCharges
                        let SID = s.Id
                        let Transcode = s.transactionCode
                        let ChargeCode = s.ChargeCode
                        let BuyCharges = s.BuyCharges
                        let SellCharges = s.SellCharges
                        let Date = s.Date
                        where s.transactionCode==p

                        select new { SID, Transcode, ChargeCode, BuyCharges, SellCharges, Date };
            return urls3;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("SpinTrans/{p}")]
        public dynamic SpinTrans2(string p)
        {
            //int? my = Convert.ToInt32(p);
            var urls3 = from s in db2.TransactionCharges
                        let SID = s.Id
                        let Transcode = s.transactionCode
                        let ChargeCode = s.ChargeCode
                        let BuyCharges = s.BuyCharges
                        let SellCharges = s.SellCharges
                        let Date = s.Date
                        where s.transactionCode == p

                        select new { SID, Transcode, ChargeCode, BuyCharges, SellCharges, Date };
            return urls3;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<CompanyEarners> GetEarners(string date)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select  c.Company_name As 'Brokername',Broker1,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(BuyCharges),0) As 'Commission','Buy' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker1 where t.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and t.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt+"') + 1, 0)) group by Broker1,c.Company_name union all"
                + " (select  c.Company_name As 'Brokername',Broker2,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(SellCharges),0) As 'Commission','Sell' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker2 where t.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and t.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt + "') + 1, 0)) group by Broker2,c.Company_name)";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CompanyEarners>();
            while (reader.Read())
            {
                var accountDetails = new CompanyEarners
                {

                   companyname = reader.GetValue(0).ToString(),
                   brokername = reader.GetValue(1).ToString(),
                   consideration=Convert.ToDecimal(reader.GetValue(2).ToString()),
                   commission= Convert.ToDecimal(reader.GetValue(3).ToString()),
                   tradeType= reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<CompanyEarners> GetEarnersRange(string date1, string date2)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date1);
            DateTime dt2 = Convert.ToDateTime(date2);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select  c.Company_name As 'Brokername',Broker1,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(BuyCharges),0) As 'Commission','Buy' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker1 where t.[Date]>='" + dt + "'  and t.[Date]<='" + dt2 + "' group by Broker1,c.Company_name union all"
                + " (select  c.Company_name As 'Brokername',Broker2,ISNULL(sum(TradePrice*TradeQty),0) As Consideration,IsNull(Sum(SellCharges),0) As 'Commission','Sell' As 'TradeType' from tblMatchedOrders o inner join transactioncharges t on CAST(o.ID as nvarchar)=t.transactioncode inner join Client_Companies c on c.Company_Code=o.Broker2 where t.[Date]>='" + dt + "'  and t.[Date]<= '" + dt2 + "' group by Broker2,c.Company_name)";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<CompanyEarners>();
            while (reader.Read())
            {
                var accountDetails = new CompanyEarners
                {

                    companyname = reader.GetValue(0).ToString(),
                    brokername = reader.GetValue(1).ToString(),
                    consideration = Convert.ToDecimal(reader.GetValue(2).ToString()),
                    commission = Convert.ToDecimal(reader.GetValue(3).ToString()),
                    tradeType = reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<ClientsDue> AmountsDue(string date)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "  select  Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames As Names ,Account2,IsNull(sum(SellCharges),0) As Amounts from tblMatchedOrders inner join TransactionCharges on CAST(tblMatchedOrders.ID as nvarchar)=TransactionCharges.transactionCode inner join Accounts_Clients on Accounts_Clients.CDS_Number=tblMatchedOrders.Account2 where TransactionCharges.[Date]>=DATEADD(q, DATEDIFF(q, 0, '" + dt + "'), 0)  and TransactionCharges.[Date]<=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, '" + dt + "') + 1, 0)) group by Account2,Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<ClientsDue>();
            while (reader.Read())
            {
                var accountDetails = new ClientsDue
                {

                    names = reader.GetValue(0).ToString(),
                    accountnumber = reader.GetValue(1).ToString(),
                    amounts = Convert.ToDecimal(reader.GetValue(2).ToString())
                   
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public List<ClientsDue> AmountsDueRange(string date1, string date2)
        {
            //return listEmp.First(e => e.ID == id); 
            DateTime dt = Convert.ToDateTime(date1);
            DateTime dt2 = Convert.ToDateTime(date2);
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DSContext"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "  select  Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames As Names ,Account2,IsNull(sum(SellCharges),0) As Amounts from tblMatchedOrders inner join TransactionCharges on CAST(tblMatchedOrders.ID as nvarchar)=TransactionCharges.transactionCode inner join Accounts_Clients on Accounts_Clients.CDS_Number=tblMatchedOrders.Account2 where TransactionCharges.[Date]>='" + dt + "' and TransactionCharges.[Date]<= '" + dt2 + "' group by Account2,Accounts_Clients.Surname +' '+ Accounts_Clients.Forenames";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<ClientsDue>();
            while (reader.Read())
            {
                var accountDetails = new ClientsDue
                {

                    names = reader.GetValue(0).ToString(),
                    accountnumber = reader.GetValue(1).ToString(),
                    amounts = Convert.ToDecimal(reader.GetValue(2).ToString())

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BrokersList")]
        [AllowAnonymous]
        public dynamic Company()
        {

            var urls4 = from s in db3.Client_Companies
                         select new { s.Company_name,s.Company_Code };

            return urls4;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Broker/{v}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public dynamic CompanyCC(string v)
        {

            var urls4 = from s in db3.Client_Companies
                        where s.Company_name==v
                        select new { s.Company_name,s.Company_Code };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("BrokerFull/{v}")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public dynamic CompanyCCC(string v)
        {

            var urls4 = from s in db3.para_company
                        where s.Company==v
                        select new { s.Fnam, s.Add_1,s.Add_2,s.Add_3 ,s.Add_4 };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Security")]
        [AllowAnonymous]
        [System.Web.Mvc.ValidateInput(false)]
        public dynamic CompanyCCCM()
        {

            var urls4 = from s in db3.para_company
                        select new { s.Fnam, s.Company };

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsList/{s}")]
        [AllowAnonymous]
        public dynamic Accounts(string s)
        {

            var urls4 = db3.Accounts_Clients_Web.ToList().Where(a=>a.BrokerCode==s);

            return urls4;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsMin/{s}")]
        [AllowAnonymous]
        public string AccountsMin(string s)
        {

            var urls4 = db3.Accounts_Clients_Web.ToList().Where(a => a.CDS_Number == s);
            string neme = "";
            foreach (var p in urls4)
            {
                neme = p.SourceName;
            }
            return neme;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsApp/{s}")]
        [AllowAnonymous]
        public string AccountsApp(string s)
        {
            s = s.Replace("-","/");
            int my=db3.BBOApprover(s);
            return my.ToString();
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("MarketWatch")]
        [AllowAnonymous]
        public dynamic AccountsM()
        {

            var sex ="";
            return sex;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("MarketWatchD/{s}")]
        [AllowAnonymous]
        public dynamic AccountsM(string s)
        {
            s = s.Replace("-",".");
            var sex = db6.Database.SqlQuery<LiveTR>("Select OrderType,IsNull(sum(Quantity),0) as 'Quantity',BasePrice from [testcds].[dbo].[LiveTradingMaster] where LOWER(Company)='"+ s +"' group by BasePrice,OrderType");
                return sex;
        }
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("AccountsListUpdate/{s}/{p}")]
        [AllowAnonymous]
        public dynamic UploadAccounts(string s,string p)
        {
            int my = Convert.ToInt32(p);
            var urls4 = db3.Accounts_Audit.ToList().Where(a => a.BrokerCode == s && a.ID==my);

            return urls4;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("OrderBooks/{s}")]
        [AllowAnonymous]
        public dynamic OrderBook(string s)
        {
            var urls4 = db4.Tbl_MatchedDeals.ToList().Where(a => a.buybroker == s || a.sellbroker == s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Executed/{s}")]
        [AllowAnonymous]
        public dynamic Executed(string s)
        {
            var urls4 = db4.Tbl_MatchedDeals.ToList().Where(a => a.BuyCompany == s || a.SellCompany == s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Outstanding/{s}")]
        [AllowAnonymous]
        public dynamic Outs(string s)
        {
            var urls4 = db4.Order_Live.ToList().Where(a => a.Broker_Code==s);

            return urls4;
        }


        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("denied/{s}")]
        [AllowAnonymous]
        public dynamic getA(string s)
        {
            var urls4 = db3.Accounts_Audit.ToList().Where(a =>a.AuthorizationState=="C");

            return urls4;
        }

   

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Order/{ordertype}/{CDS_Number}/{NoofNotes}/{TelephoneSelorder}/{Company}/{UserName}/{PassWord}/{MNO_}/{BasePrice}/{BeginDate}/{ExpiryDate}/{BrokerCode}/{MaxPrice}/{MiniPrice}/{shareholder}/{client}/{curr}/{tax}/{tp}")]
        [AllowAnonymous]
        public string PostOrder(string ordertype,string CDS_Number, string NoofNotes, string TelephoneSelorder, string Company, string UserName, string PassWord, string MNO_, string BasePrice,string BeginDate ,string ExpiryDate,String BrokerCode,string MaxPrice,string MiniPrice,string shareholder,string client,string curr,string tax,string tp)
        {
            string response = "";
            string responseid = "";
            long max = 0;

            DateTime? my11 = Convert.ToDateTime(BeginDate);
            DateTime? my2 = Convert.ToDateTime(ExpiryDate);
            try
            {
                max = db4.Pre_Order_Live.ToList().Max(a => a.OrderNo);

            }
            catch (Exception)
            {

                max = 0;
            }
            max += 1;

            try
            {
                Pre_Order_Live my = new Pre_Order_Live();
                my.OrderType = ordertype.ToUpper();
                my.Company = Company;
                my.SecurityType = "EQUITY";
                my.CDS_AC_No = CDS_Number.Replace("-","/");
                my.Broker_Code = BrokerCode;
                my.Client_Type = "";
                my.Tax = 0;
                my.Shareholder = shareholder.Replace("-", "/");
                my.ClientName = client.Replace("-", "/");
                my.OrderStatus = "OPEN";
                my.Create_date = DateTime.Now;
                my.Deal_Begin_Date = my11;
                my.Expiry_Date = my2;
                my.Quantity = Convert.ToInt32(NoofNotes);
                my.BrokerRef = my.Broker_Code + "Ord"+(max - 1).ToString();
                my.OrderNumber = max.ToString();
                my.BasePrice = Convert.ToDouble(BasePrice);
                my.AvailableShares = 0;
                my.OrderPref = "L";
                my.OrderAttribute = "";
                my.Marketboard = "Normal Board";
                my.TimeInForce =curr;
                my.OrderQualifier = "None";
                my.ContraBrokerId = "None";
                my.MaxPrice = 0;
                my.MiniPrice =0;
                my.Flag_oldorder = false;
                my.TotalShareHolding = 0;
                my.Currency = "USD";
                my.Affirmation = true;
                my.trading_platform = tp;
                my.FOK = Convert.ToBoolean(tax);
                db4.Pre_Order_Live.Add(my);
                db4.SaveChanges();

                try
                {
                    var d = db4.Order_Live_Responses.ToList().Where(a => a.OrderNo == max);
foreach(var p in d)
                {
                    responseid = p.OrderNo.ToString();
                    response = p.Failed_Reason;
                }
                }
                catch (Exception)
                {

                   
                }
                
                if (responseid =="")
                {
                    responseid = max.ToString();
                    response = "Order number "+ max.ToString() +" has been processed sucessfully";
                }
                return responseid+","+response;
            }
            catch (Exception f)
            {

                return f.ToString();
            }
           

        }
        [System.Web.Http.AcceptVerbs("PUT")]
        [System.Web.Http.HttpPut]
        [Route("OrderUpdate/{ordertype}/{CDS_Number}/{NoofNotes}/{TelephoneSelorder}/{Company}/{UserName}/{PassWord}/{MNO_}/{BasePrice}/{BeginDate}/{ExpiryDate}/{BrokerCode}/{MaxPrice}/{MiniPrice}/{shareholder}/{client}/{orderNo}/{curr}/{tax}")]
        [AllowAnonymous]
        public string PostOrderUpdate(string ordertype, string CDS_Number, string NoofNotes, string TelephoneSelorder, string Company, string UserName, string PassWord, string MNO_, string BasePrice, string BeginDate, string ExpiryDate, String BrokerCode, string MaxPrice, string MiniPrice, string shareholder, string client,string orderNo,string curr,string tax)
        {
            //string response = "";
            long cs = Convert.ToInt32(orderNo);
            long max = 0;
            try
            {
                max = db4.Order_Live.ToList().Where(a => a.OrderNo == cs).Count();

            }
            catch (Exception)
            {

                max = 0;
            }
            DateTime? my11 = Convert.ToDateTime(BeginDate);
            DateTime? my2 = Convert.ToDateTime(ExpiryDate);
            string base1 = BasePrice.ToString().Replace("T", ".");
            string max2 = MaxPrice.ToString().Replace("T", ".");
            string mini2 = MiniPrice.ToString().Replace("T", ".");
            string tax2 = tax.ToString().Replace("T", ".");
            if (max > 0)
            {
                
                    Order_Live my = db4.Order_Live.Find(cs);
                my.OrderType = ordertype.ToUpper();
                my.Company = Company;
                    my.SecurityType = "EQUITY";
                    my.CDS_AC_No = CDS_Number;
                    my.Broker_Code = BrokerCode;
                    my.Client_Type = "N/A";
                    my.Tax = 0;
                    my.Shareholder = shareholder;
                    my.ClientName = client;
                    my.OrderStatus = "OPEN";
                    my.Deal_Begin_Date = my11;
                    my.Expiry_Date = my2;
                    my.Quantity = Convert.ToInt32(NoofNotes);
                    my.BrokerRef = my.BrokerRef;
                    my.OrderNumber = orderNo;
                    my.BasePrice = Convert.ToDecimal(base1);
                    my.AvailableShares = 0;
                    my.OrderPref = my.OrderPref;
                    my.OrderAttribute =my.OrderAttribute;
                    my.Marketboard = "Normal Board";
                    my.TimeInForce = "Day Order (DO)";
                    my.OrderQualifier = "None";
                    my.ContraBrokerId = "None";
                my.MaxPrice = 0;
                my.MiniPrice = 0;
                my.Flag_oldorder = false;
                my.TotalShareHolding = 0;
                my.Currency = "USD";
                my.Affirmation = true;
                my.FOK = false;
                db4.Order_Live.AddOrUpdate(my);
                    db4.SaveChanges();
                    return orderNo;
              
            }
            else
            {
                return "Matched";
            }
        }


        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Test/{s}")]
        [AllowAnonymous]

        public dynamic good(string s)
        {
            //string my = "sex";
            var url = "it works" + s;
            return url;
        }

        [System.Web.Http.AcceptVerbs("PUT")]
        [System.Web.Http.HttpPut]
        [Route("place/{s}")]
        [AllowAnonymous]
        public dynamic good2(string s)
        {
            DateTime? my = Convert.ToDateTime(s);
            var url = "it works" + s;
            return url;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("CheckOrder/{s}")]
        [AllowAnonymous]
        public string checkorders(string s)
        {

            int mys = Convert.ToInt32(s);
            int d = 0;
            try
            {
                d = db4.Order_Live.ToList().Where(a => a.OrderNo == mys).Count();
            }
            catch (Exception)
            {
                d = 0;
            }
           
            return d.ToString();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Orders/{s}")]
        [AllowAnonymous]
        public string orders(string s)
        {

            int mys = Convert.ToInt32(s);
            int d = 0;
            try
            {
                d = db4.Order_Live.ToList().Where(a => a.OrderNo == mys).Count();
            }
            catch (Exception)
            {
                d = 0;
            }

            return d.ToString();
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("Billing")]
        [AllowAnonymous]
        public dynamic updates()
        {

            var c = db3.para_Billing.ToList();

            return c;
        }

       
        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Joint/{Surname}/{Forenames}/{IDNo}/{IDType}/{Nationality}/{DateOfBirth}/{Gender}/{CDSNo}/{email}")]
        [AllowAnonymous]
        public string AccountJoint(string Surname, string Forenames, string IDNo, string IDType, string Nationality, string DateOfBirth, string Gender, string CDSNo, string email)
        {
            string message = "Not Saved";
            int d = 0;

            try
            {
                d = db3.Accounts_Joint.ToList().Where(a => a.Forenames == Forenames && a.Surname == Surname && a.IDNo == IDNo && a.CDSNo == CDSNo).Count();

            }
            catch (Exception)
            {

                d = 0;
            }
           
            if (d == 0)
            {
                Accounts_Joint p = new Accounts_Joint();
                p.Forenames = Forenames;
                p.Surname = Surname;
                p.DateOfBirth = Convert.ToDateTime(DateOfBirth);
                p.Gender = Gender;
                p.IDNo = IDNo;
                p.IDType = IDType;
                p.email = email.ToString().Replace("-", ".");
                p.Nationality = Nationality;
                p.CDSNo = CDSNo;
                db3.Accounts_Joint.AddOrUpdate(p);
                db3.SaveChanges();
                message = "Successfully";
            }
            return message;
        }

        //Getting stufff from IPO
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("IPAgents")]
        [AllowAnonymous]
        public dynamic ipagents()
        {

            var c = from s in db5.para_agent
                    select new {s.desc,s.code };

            return c;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("IPCompany")]
        [AllowAnonymous]
        public dynamic ipcompany()
        {

            var c = from s in db5.para_main
                    select new {s.company_name,s.company};

            return c;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("IPPCompany/{f}")]
        [AllowAnonymous]
        public string ipcompany(string f)
        {

            var c = from s in db5.para_main
                   where s.company==f
                    select new {s.price };
            string pr = "";
            foreach (var p in c)
            {
                pr = p.price.ToString();
            }
            return pr;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("IPPPCompany/{f}")]
        [AllowAnonymous]
        public string ippcompany(string f)
        {

            var c = from s in db5.para_main
                    where s.company == f
                    select new { s.min_app };
            string pr = "";
            foreach (var p in c)
            {
                pr = p.min_app.ToString();
            }
            return pr;
        }

        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Batchn/{batchno}/{shrs}/{amt}/{comp}/{agg}/{user}/{numapps}")]
        [AllowAnonymous]
        public string submitbatch(string batchno,string shrs,string amt,string comp,string agg,string user,string numapps)
        { string nme = "";
            try
            {
                var pz = db5.batch_mast.ToList().Where(a => a.batch_no == batchno).Count();
               
                if (pz == 0)
                {



                    batch_mast tt = new batch_mast();
                    tt.batch_no = batchno;
                    tt.batch_type = "BBO";
                    tt.app_shares = Convert.ToDecimal(shrs);
                    tt.app_cash = Convert.ToDecimal(amt);
                    tt.status = "C";
                    tt.Company = comp;
                    tt.Agent = agg;
                    tt.BrokerBatchRef = tt.Agent + "//" + tt.batch_no;
                    tt.Verify = false;
                    tt.CreatedBy = user;
                    tt.apps = numapps;
                    tt.branch = "N/A";
                    tt.datecreated = DateTime.Now;
                    tt.PayMode = "N/A";
                    tt.BoxNo = 0;
                    tt.OldBatchRef = "N/A";
                    tt.Source = "BBO";
                    db5.batch_mast.AddOrUpdate(tt);
                    db5.SaveChanges();

                    nme = "The batch was successfully created.";

                }
                if (pz > 0)
                {

                    nme = "Duplicate batch numbers are not allowed in IPO";
                }
            }
            catch (Exception z)
            {

                nme = z.Message.ToString();
            }

            return nme;
        }

        //insert into batchdets
        [System.Web.Http.AcceptVerbs("POST")]
        [System.Web.Http.HttpPost]
        [Route("Batchnapp/{batchno}/{shrs}/{amt}/{comp}/{agg}/{user}/{cds}/{idno}")]
        [AllowAnonymous]
        public dynamic submitbatchapp(string batchno, string shrs, string amt, string comp, string agg, string user,string cds,string idno)
        {

            try
            {
                var p = db3.Accounts_Clients.ToList().Where(a=>a.CDS_Number==cds);
                foreach (var k in p)
                {
                    batch_dets tt = new batch_dets();
                    tt.batch_no = batchno;
                    tt.app_no = idno;
                    tt.batchref = agg + "//" + batchno;
                    tt.broker = agg;
                    tt.Agent = agg;
                    tt.shareholder = cds;
                    tt.MemCode = agg;
                    tt.Industry = "LI";
                    tt.Citizenship = k.Country;
                    tt.surname = k.Surname;
                    tt.forenames = k.Forenames;
                    tt.title = k.Title;
                    if (k.AccountType == "I")
                    {
                        tt.AccountType = "INDIVIDUAL";
                    }
                    else if (k.AccountType == "J")
                    {
                        tt.AccountType = "JOINT";
                    }
                    else if (k.AccountType == "C")
                    {
                        tt.AccountType = "CORPORATE";
                    }

                    tt.IDType = k.IDtype;
                    tt.IDReg = k.IDNoPP;
                    tt.add_1 = k.Add_1;
                    tt.mobile = k.mobile_number;

                    tt.Country = k.Country;
                    tt.email = k.Email;
                    tt.Tel = k.Tel;
                    tt.app_shares = Convert.ToDecimal(shrs);
                    tt.app_cash = Convert.ToDecimal(amt);
                    tt.scale_shares = Convert.ToDecimal(shrs);
                    tt.scale_cash = Convert.ToDecimal(amt);
                    tt.refund = 0;

                    tt.pay_type = "C";
                    tt.Verify = false;
                    tt.eft_bank = k.Div_Bank;
                    tt.eft_branch = k.Div_Branch;
                    tt.eft_account = k.Div_AccountNo;
                    tt.EFT2 = k.Div_Bank;
                    tt.eft_bank = k.Div_Bank;
                    tt.eft_branch1 = k.Div_Branch;
                    tt.eft_account1 = k.Div_AccountNo;
                    tt.company = comp;
                    tt.seq = 0;
                    tt.Cert = 0;
                    tt.cheq_no = "0";
                    tt.chequestatus = "0";
                    tt.DateCaptured = DateTime.Now;
                    tt.CreatedBy = user;
                    tt.CDSACCount = k.CDS_Number;
                    tt.Account_Holder = k.Surname + " " + k.Forenames;
                    db5.batch_dets.Add(tt);
                    db5.SaveChanges();
                }
                return "Successful";
            }
            catch (Exception f)
            {

                return f.Message;
            }
            //return "Not Successful";
        }



        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("securitytotals/{broker}/{company}")]
        [AllowAnonymous]
        public List<securitytotals> markeets(string broker, string company)
        {

            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["CDSR"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "declare @custodian nvarchar(50)='" + broker + "';declare @company nvarchar(50)='" + company + "'; select (select count(*) from accounts_clients where custodian=@custodian) AS [TotalRegistration] ,ISNULL((select sum(amount) from tbl_cashbalance where clientid in (select cds_number from accounts_clients where custodian=@custodian)),0) as [totalfunds], (select sum(shares) from trans where cds_number in (select cds_number from accounts_clients where custodian=@custodian) and company=@company) as [totalholdings], 0 AS CDCHOLDINGS, ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and company=@company and OrderType='SELL'),0) AS [Incomingsells],ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and company=@company and OrderType='BUY'),0) AS [Incomingbuys],(  select isnull(sum(tradeqty),0) from tblmatchedorders where banksent='0' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and company=@company) as [Pendingsettlement],(  select isnull(sum(tradeqty),0) from tblmatchedorders where ack='SETTLED' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and company=@company and convert(date,SettlementDate)=convert(date,getdate()))  as [todaysettlement]";
            if (company=="")
            {
            sqlCmd.CommandText = "declare @custodian nvarchar(50)='" + broker + "'; select (select count(*) from accounts_clients where custodian=@custodian) AS [TotalRegistration] ,ISNULL((select sum(amount) from tbl_cashbalance where clientid in (select cds_number from accounts_clients where custodian=@custodian)),0) as [totalfunds], (select sum(shares) from trans where cds_number in (select cds_number from accounts_clients where custodian=@custodian)) as [totalholdings], 0 AS CDCHOLDINGS, ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and OrderType='SELL'),0) AS [Incomingsells],ISNULL((SELECT SUM(quantity) FROM TESTCDS.DBO.LIVETRADINGMASTER  where cds_ac_no in (select cds_number from accounts_clients where custodian=@custodian) and OrderType='BUY'),0) AS [Incomingbuys],(  select isnull(sum(tradeqty),0) from tblmatchedorders where banksent='0' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian))) as [Pendingsettlement],(  select isnull(sum(tradeqty),0) from tblmatchedorders where ack='SETTLED' and (account1 in (select cds_number from accounts_clients where custodian=@custodian) or account2 in (select cds_number from accounts_clients where custodian=@custodian)) and convert(date,SettlementDate)=convert(date,getdate()))  as [todaysettlement]";

            }
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<securitytotals>();
            while (reader.Read())
            {
                var accountDetails = new securitytotals
                {


                    TotalRegistration = reader.GetValue(0).ToString(),
                    totalfunds = reader.GetValue(1).ToString(),
                    CDCHOLDINGS = reader.GetValue(2).ToString(),
                    totalholdings = reader.GetValue(3).ToString(),
                    Incomingsells = reader.GetValue(4).ToString(),
                    Incomingbuys = reader.GetValue(5).ToString(),
                    Pendingsettlement = reader.GetValue(6).ToString(),
                    todaysettlement = reader.GetValue(7).ToString()

                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;

        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("registrations/{s}")]
        [AllowAnonymous]
        public List<Brokers> registrations(string s)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["CDSR"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select cds_number, brokercode,  left(isnull(surname,'')+' '+isnull(forenames,''),20) as [fullname], idnopp, custodian, mobile,CASE WHEN IDtype='NID' Then 'NATIONAL ID' WHEN IDType='National ID' Then 'NATIONAL ID' Else IDtype End as 'IDType',CASE WHEN AccountType='I' THEN 'INDIVIDUAL' WHEN AccountType='J'  THEN 'JOINT' WHEN AccountType='C' THEN 'CORPORATE' ELSE AccountType END AS 'AccountType',TradingStatus  from accounts_clients where BrokerCode='" + p + "' ORDER BY ID DESC";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Brokers>();
            while (reader.Read())
            {
                var accountDetails = new Brokers
                {

                    Name = reader.GetValue(2).ToString(),
                    CDSNo = reader.GetValue(0).ToString(),
                    Phone = reader.GetValue(5).ToString(),
                    Broker = reader.GetValue(1).ToString(),
                    IDNumber = reader.GetValue(3).ToString(),
                    IDType = reader.GetValue(6).ToString(),
                    AccountType = reader.GetValue(7).ToString(),
                    TradingStatus = reader.GetValue(8).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("marketwatch/{s}")]
        [AllowAnonymous]
        public List<Brokers> markets(string p)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["CDSR"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select cds_number, brokercode,  left(isnull(surname,'')+' '+isnull(forenames,''),20) as [fullname], idnopp, custodian, mobile,CASE WHEN IDtype='NID' Then 'NATIONAL ID' WHEN IDType='National ID' Then 'NATIONAL ID' Else IDtype End as 'IDType',CASE WHEN AccountType='I' THEN 'INDIVIDUAL' WHEN AccountType='J'  THEN 'JOINT' WHEN AccountType='C' THEN 'CORPORATE' ELSE AccountType END AS 'AccountType',TradingStatus  from accounts_clients where BrokerCode='" + p + "' ORDER BY ID DESC";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<Brokers>();
            while (reader.Read())
            {
                var accountDetails = new Brokers
                {

                    Name = reader.GetValue(2).ToString(),
                    CDSNo = reader.GetValue(0).ToString(),
                    Phone = reader.GetValue(5).ToString(),
                    Broker = reader.GetValue(1).ToString(),
                    IDNumber = reader.GetValue(3).ToString(),
                    IDType = reader.GetValue(6).ToString(),
                    AccountType = reader.GetValue(7).ToString(),
                    TradingStatus = reader.GetValue(8).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("marketstatus")]
        [AllowAnonymous]
        public string marketstatus()
        {
            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            DataSet ds = new DataSet();
            int i = 0;
            cmd = new SqlCommand("select * from marketstatus", myConnection);
            adp = new SqlDataAdapter(cmd);
            adp.Fill(ds, "stats");
            string stat = "";
            foreach (DataRow myRow in ds.Tables[0].Rows)
            {
                stat = myRow["Market Status"].ToString();

            }
            return stat;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("loadmarketwatch")]
        [AllowAnonymous]
        public List<marketwatch> marketsss()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select *, (select bestprice from companyprices where company=marketwatcher.company) as [Last Traded Price], isnull((select top 1 dealprice from tbl_matcheddeals where BuyCompany=marketwatcher.company  order by id desc),0) as [Lastmatched],isnull((select top 1 Quantity from tbl_matcheddeals where BuyCompany=marketwatcher.company  order by id desc),0) as [lastvolume], isnull((select sum(Quantity) from tbl_matcheddeals where BuyCompany=marketwatcher.company And trade=convert(date,getdate())),0) as [TotalVolume] ,isnull((select sum(Quantity*DealPrice) from tbl_matcheddeals where BuyCompany=marketwatcher.company And trade=convert(date,getdate())),0) as [Turnover], (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company) as [Open],isnull((select  max(DealPrice) from tbl_matcheddeals where BuyCompany= marketwatcher.company and trade=convert(date,getdate())),0) as [High],isnull((select  min(DealPrice) from tbl_matcheddeals where BuyCompany= marketwatcher.company and trade=convert(date,getdate())),0) as [Low], (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company) as [Average Price],(select top 1 bestprice from CompanyPrices where company=marketwatcher.company) - (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company) as [Change],((select top 1 bestprice from CompanyPrices where company=marketwatcher.company) - (select top 1 OpeningPrice from CompanyPrices where company=marketwatcher.company))/(select top 1 openingprice from CompanyPrices where company=marketwatcher.company)*100 as [percchange], case when (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company)>(select top 1 OpeningPrice  from CompanyPrices where company=marketwatcher.company) then '~/images/up.png'  when (select top 1 BestPrice from CompanyPrices where company=marketwatcher.company)<(select top 1 OpeningPrice  from CompanyPrices where company=marketwatcher.company) then '~/images/down.png' else '~/images/nochange.png' end  as url, '~/images/yellow.gif' as url2 from marketwatcher";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<marketwatch>();
            while (reader.Read())
            {
                var accountDetails = new marketwatch
                {
                    company = reader.GetValue(0).ToString(),
                    Volume = reader.GetValue(1).ToString(),
                    Bid = reader.GetValue(2).ToString(),
                    VolumeSell = reader.GetValue(3).ToString(),
                    Ask = reader.GetValue(4).ToString(),
                    LastTradedPrice = reader.GetValue(5).ToString(),
                    Lastmatched = reader.GetValue(6).ToString(),
                    lastvolume = reader.GetValue(7).ToString(),
                    TotalVolume = reader.GetValue(8).ToString(),
                    Turnover = reader.GetValue(9).ToString(),
                    Open = reader.GetValue(10).ToString(),
                    High = reader.GetValue(11).ToString(),
                    Low = reader.GetValue(12).ToString(),
                    AveragePrice = reader.GetValue(13).ToString(),
                    Change = reader.GetValue(14).ToString(),
                    percchange = reader.GetValue(15).ToString(),
                    url = reader.GetValue(16).ToString(),
                    url2 = reader.GetValue(17).ToString()
                    
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("marketwatchzse")]
        [AllowAnonymous]
        public List<marketwatchzse> marketszse()
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["CDSR"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "SELECT [Ticker],[ISIN],[Best_Ask],[Best_bid],[Current_price]  FROM [CDS_ROUTER].[dbo].[ZSE_market_data]";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<marketwatchzse>();
            while (reader.Read())
            {
                var accountDetails = new marketwatchzse
                {

                    Ticker = reader.GetValue(0).ToString(),
                    ISIN = reader.GetValue(1).ToString().Replace(" ","").Trim(),
                    Best_Ask = reader.GetValue(2).ToString(),
                    Best_bid = reader.GetValue(3).ToString(),
                    Current_price = reader.GetValue(4).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("loadbuys/{company}/{broker}")]
        [AllowAnonymous]
        public List<loadbuys> marketbuys(string company, string broker)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Company, Quantity, BasePrice, right(OrderPref,5) as OrderPref, Timeinforce AS [TIF], FOK as [Block], case broker_code when '" + broker + "' then  '~/images/yellow.gif' else NULL end as [mine] from (select case when baseprice=0 then 1 else 2 end as PriceRow, Company, Quantity, Baseprice, OrderPref, Timeinforce, FOK, orderdate, broker_code  from Livetradingmaster where ordertype='BUY') j where j.company='" + company + "' order by BASEPRICE desc, j.orderdate asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<loadbuys>();
            while (reader.Read())
            {
                var accountDetails = new loadbuys
                {

                    Company = reader.GetValue(0).ToString(),
                    Quantity = reader.GetValue(1).ToString(),
                    BasePrice = reader.GetValue(2).ToString(),
                    OrderPref = reader.GetValue(3).ToString(),
                    TIF = reader.GetValue(4).ToString(),
                    Block = reader.GetValue(5).ToString(),
                    mine = reader.GetValue(6).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        [Route("loadsells/{company}/{broker}")]
        [AllowAnonymous]
        public List<loadsells> marketsells(string company, string broker)
        {
            //return listEmp.First(e => e.ID == id); 

            SqlDataReader reader = null;
            SqlConnection myConnection = new SqlConnection();
            myConnection.ConnectionString = ConfigurationManager.ConnectionStrings["ATS"].ConnectionString;

            SqlCommand sqlCmd = new SqlCommand();
            sqlCmd.CommandType = CommandType.Text;
            sqlCmd.CommandText = "select Company, Quantity, BasePrice, right(OrderPref,5) as OrderPref, Timeinforce AS [TIF], FOK as [Block], case broker_code when '" + broker + "' then  '~/images/yellow.gif' else NULL end as [mine] from (select case when baseprice=0 then 1 else 2 end as PriceRow, Company, Quantity, Baseprice, OrderPref, Timeinforce, FOK, orderdate, broker_code  from Livetradingmaster where ordertype='SELL') j where j.company='" + company + "' order by BASEPRICE asc, j.orderdate asc";
            sqlCmd.Connection = myConnection;
            myConnection.Open();
            sqlCmd.CommandTimeout = 0;
            reader = sqlCmd.ExecuteReader();
            //GetBalance emp = null;
            var accDetails = new List<loadsells>();
            while (reader.Read())
            {
                var accountDetails = new loadsells
                {

                    Company = reader.GetValue(0).ToString(),
                    Quantity = reader.GetValue(1).ToString(),
                    BasePrice = reader.GetValue(2).ToString(),
                    OrderPref = reader.GetValue(3).ToString(),
                    TIF = reader.GetValue(4).ToString(),
                    Block = reader.GetValue(5).ToString(),
                    mine = reader.GetValue(6).ToString()
                };
                accDetails.Add(accountDetails);


            }

            myConnection.Close();

            return accDetails;
        }

    }
}
